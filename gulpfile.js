var elixir = require('laravel-elixir')
elixir(function (mix) {
    mix.sass('app.scss')
    mix.styles([
        '../../../public/css/app.css',
        'style.css'
    ], 'public/assets/css/app.css')
    mix.scripts([
        '../../../node_modules/jquery/dist/jquery.js',
        '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js',
        'app.js'
    ], 'public/assets/js/all.js')
    mix.version(['assets/css/app.css', 'assets/js/all.js'])
    mix.copy('node_modules/font-awesome/fonts', 'public/fonts')
    mix.copy('node_modules/font-awesome/fonts', 'public/build/fonts')
})