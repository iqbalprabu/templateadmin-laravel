<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Image;
use Redirect;
class ImageController extends Controller
{
    public function store(Request $request)
    {

        if($request->hasFile('imagefile')) {
            $file = $request->file('imagefile');

            $filename = date('ymd').str_random(30).$file->getClientOriginalName();

            $file->move('uploads/', $filename);

            $dirname = url('uploads').'/'.$filename;
        }
        return view('form._image-upload', compact('dirname'));
    }
}
