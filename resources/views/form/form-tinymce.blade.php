@extends('section.app')
@section('content')
    <div class="ai-content">
        <div class="page-head">
            <h2 class="page-head-title">Form Tinymce</h2>
        </div>
        <div class="main-content container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Form Tinymce<span class="panel-subtitle">Beautiful Text Editor</span></div>
                        <div class="panel-body">
                            <textarea name="body" class="form-control tinymce"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ url('assets/lib/tinymce/jquery.tinymce.min.js') }}" type="text/javascript"></script>
    <script src="{{ url('assets/lib/tinymce/tinymce.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            tinymce.init({
                menubar : false,
                selector: ".tinymce",
                height: 500,
                plugins: [
                    "link image code imageupload"
                ],
                toolbar: 'insertfile undo redo | link styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | imageupload',
                relative_urls: false
            });
        })
    </script>
@endsection