@extends('section.app')
@section('content')
    <div class="ai-content">
        <div class="page-head">
            <h2 class="page-head-title">Form Tinymce</h2>
        </div>
        <div class="main-content container-fluid">
            <form id="my-awesome-dropzone" action="assets/lib/dropzone/upload.php" class="dropzone">
                <div class="dz-message">
                    <div class="icon"><span class="zmdi zmdi-cloud-upload"></span></div>
                    <h2>Drag and Drop files here</h2><span class="note">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</span>
                </div>
            </form>
        </div>
    </div>
@endsection