@extends('section.app')
@section('content')
    <div class="ai-content">
        <div class="page-head">
            <h2 class="page-head-title">Form General</h2>
        </div>
        <div class="main-content container-fluid">
            <!--Basic forms-->
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Basic Form<span class="panel-subtitle">This is the default bootstrap form layout</span></div>
                        <div class="panel-body">
                            {!! Form::open(['url' => '/form-general', 'method' => 'get']) !!}
                                <div class="form-group xs-pt-10">
                                    {!! Form::label('email', 'Email Adress') !!}
                                    {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email Adress']) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password', 'Password') !!}
                                    {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                                </div>
                                <div class="row xs-pt-15">
                                    <div class="col-xs-6">
                                        <div class="ai-checkbox">
                                            {!! Form::checkbox('remember-me', 'Remember Me', false, ['id' => 'rememberme']) !!}
                                            {!! Form::label('rememberme', 'Remember Me') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <p class="text-right">
                                            {!! Form::submit('Submit', ['class' => 'btn btn-space btn-primary']) !!}
                                            {!! Form::button('Cancel', ['class' => 'btn btn-space btn-default']) !!}
                                        </p>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Horizontal Form<span class="panel-subtitle">This is the horizontal bootstrap layout</span></div>
                        <div class="panel-body">
                            {!! Form::open(['url' => '/form-general', 'method' => 'get', 'class' => 'form-horizontal']) !!}
                                <div class="form-group xs-mt-10">
                                    {!! Form::label('email', 'Email Adress', ['class' => 'col-sm-2 control-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email Adress']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password', 'Password', ['class' => 'col-sm-2 control-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) !!}
                                    </div>
                                </div>
                                <div class="row xs-pt-15">
                                    <div class="col-xs-6">
                                        <div class="ai-checkbox">
                                            {!! Form::checkbox('remember-me', 'Remember Me', false, ['id' => 'rememberme']) !!}
                                            {!! Form::label('rememberme', 'Remember Me') !!}
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <p class="text-right">
                                            {!! Form::submit('Submit', ['class' => 'btn btn-space btn-primary']) !!}
                                            {!! Form::button('Cancel', ['class' => 'btn btn-space btn-default']) !!}
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Basic Elements-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Basic Elements<span class="panel-subtitle">These are the basic bootstrap form elements</span></div>
                        <div class="panel-body">
                            {!! Form::open(['url' => '#', 'method' => 'get', 'class' => 'form-horizontal group-border-dashed', 'style' => 'border-radius: 0px;']) !!}
                                <div class="form-group">
                                    {!! Form::label('text', 'Input Text', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::text('text', null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('password', 'Input Password', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::password('password', ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('text', 'Placeholder Text', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::text('text', null, ['class' => 'form-control', 'placeholder' => 'Placeholder Text']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('text', 'Disabled Input Text', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::text('text', null, ['class' => 'form-control', 'placeholder' => 'Disabled Input Text', 'disabled' => 'disabled']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('text', 'Readonly Input Text', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::text('text', null, ['class' => 'form-control', 'placeholder' => 'Readonly Input Text', 'readonly' => 'readonly']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('textarea', 'Textarea', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::textarea('textarea', null, ['class' => 'form-control', 'rows' => 5]) !!}
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!--Sizing-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Input Sizing<span class="panel-subtitle">These are the different form control component sizes</span></div>
                        <div class="panel-body">
                            {!! Form::open(['url' => '#', 'method' => 'get', 'class' => 'form-horizontal group-border-dashed', 'style' => 'border-radius: 0px;']) !!}
                                <div class="form-group">
                                    {!! Form::label('large', 'Large', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::text('large', 'Large Input', ['class' => 'form-control input-lg']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('default', 'Default', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::text('default', 'Default Input', ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('small', 'Small', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::text('small', 'Small Input', ['class' => 'form-control input-sm']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('extrasmall', 'Extra Small', ['class' => 'col-sm-3 control-label']) !!}
                                    <div class="col-sm-6 xs-pt-5">
                                        {!! Form::text('extrasmall', 'Extra Small Input', ['class' => 'form-control input-xs']) !!}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Select & Option Elements<span class="panel-subtitle">Advanced custom radio & checkboxes components with pure css</span></div>
                        <div class="panel-body">
                            <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label xs-pt-20">Icon Radio</label>
                                    <div class="col-sm-6">
                                        <div class="ai-radio-icon inline">
                                            {!! Form::radio('rad1', 'female', true, ['id' => 'rad1']) !!}
                                            <label for="rad1"><span class="zmdi zmdi-female"></span></label>
                                        </div>
                                        <div class="ai-radio-icon inline">
                                            {!! Form::radio('rad1', 'male', false, ['id' => 'rad2']) !!}
                                            <label for="rad2"><span class="zmdi zmdi-male-alt"></span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Checkbox</label>
                                    <div class="col-sm-6">
                                        <div class="ai-checkbox">
                                            {!! Form::checkbox('check1', 'Option 1', false, ['id' => 'check3']) !!}
                                            {!! Form::label('check3', 'Option 1') !!}
                                        </div>
                                        <div class="ai-checkbox">
                                            {!! Form::checkbox('check1', 'Option 2', true, ['id' => 'check4']) !!}
                                            {!! Form::label('check4', 'Option 2') !!}
                                        </div>
                                        <div class="ai-checkbox">
                                            {!! Form::checkbox('check1', 'Option 3', true, ['id' => 'check4']) !!}
                                            {!! Form::label('check4', 'Option 3') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Inline Checkbox</label>
                                    <div class="col-sm-6">
                                        <div class="ai-checkbox inline">
                                            {!! Form::checkbox('check1', 'Option 1', true, ['id' => 'check3']) !!}
                                            {!! Form::label('check3', 'Option 1') !!}
                                        </div>
                                        <div class="ai-checkbox inline">
                                            {!! Form::checkbox('check1', 'Option 1', false, ['id' => 'check3']) !!}
                                            {!! Form::label('check3', 'Option 2') !!}
                                        </div>
                                        <div class="ai-checkbox inline">
                                            {!! Form::checkbox('check1', 'Option 1', false, ['id' => 'check3']) !!}
                                            {!! Form::label('check3', 'Option 3') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Full Color</label>
                                    <div class="col-sm-6">
                                        <div class="ai-checkbox ai-checkbox-color inline">
                                            {!! Form::checkbox('check1', 'Option 1', false, ['id' => 'check3']) !!}
                                            {!! Form::label('check3', 'Option 1') !!}
                                        </div>
                                        <div class="ai-checkbox ai-checkbox-color inline">
                                            {!! Form::checkbox('check1', 'Option 1', false, ['id' => 'check3']) !!}
                                            {!! Form::label('check3', 'Option 2') !!}
                                        </div>
                                        <div class="ai-checkbox ai-checkbox-color inline">
                                            {!! Form::checkbox('check1', 'Option 1', true, ['id' => 'check3']) !!}
                                            {!! Form::label('check3', 'Option 3') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Radio</label>
                                    <div class="col-sm-6">
                                        <div class="ai-radio">
                                            {!! Form::radio('rad2', 'rad1', true, ['id' => 'rad3']) !!}
                                            {!! Form::label('rad3', 'Option 1') !!}
                                        </div>
                                        <div class="ai-radio">
                                            {!! Form::radio('rad2', 'rad4', false, ['id' => 'rad4']) !!}
                                            {!! Form::label('rad4', 'Option 2') !!}
                                        </div>
                                        <div class="ai-radio">
                                            {!! Form::radio('rad2', 'rad5', false, ['id' => 'rad5']) !!}
                                            {!! Form::label('rad5', 'Option 3') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Inline Radio</label>
                                    <div class="col-sm-6">
                                        <div class="ai-radio inline">
                                            {!! Form::radio('rad3', 'rad1', true, ['id' => 'rad6']) !!}
                                            {!! Form::label('rad6', 'Option 1') !!}
                                        </div>
                                        <div class="ai-radio inline">
                                            {!! Form::radio('rad3', 'rad4', false, ['id' => 'rad7']) !!}
                                            {!! Form::label('rad7', 'Option 2') !!}
                                        </div>
                                        <div class="ai-radio inline">
                                            {!! Form::radio('rad3', 'rad5', false, ['id' => 'rad8']) !!}
                                            {!! Form::label('rad8', 'Option 3') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Full Color</label>
                                    <div class="col-sm-6">
                                        <div class="ai-radio ai-radio-color inline">
                                            {!! Form::radio('rad4', 'rad1', true, ['id' => 'rad9']) !!}
                                            {!! Form::label('rad9', 'Option 1') !!}
                                        </div>
                                        <div class="ai-radio inline">
                                            {!! Form::radio('rad4', 'rad4', false, ['id' => 'rad10']) !!}
                                            {!! Form::label('rad10', 'Option 2') !!}
                                        </div>
                                        <div class="ai-radio inline">
                                            {!! Form::radio('rad4', 'rad5', false, ['id' => 'rad11']) !!}
                                            {!! Form::label('rad11', 'Option 3') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Basic Select</label>
                                    <div class="col-sm-6">
                                        {!! Form::select('size', ['1' => '1', '2' => '2', '3' => '3'], null, ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Multiple Select</label>
                                    <div class="col-sm-6">
                                        {!! Form::select('size', ['1' => '1', '2' => '2', '3' => '3'], null, ['class' => 'form-control', 'multiple' => 'multiple']); !!}
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Validation States-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Validation States<span class="panel-subtitle">Default bootstrap validation states</span></div>
                        <div class="panel-body">
                            <form action="#" class="form-horizontal group-border-dashed">
                                <div class="form-group has-success">
                                    <label class="col-sm-3 control-label">Success</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group has-warning">
                                    <label class="col-sm-3 control-label">Warning</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group has-error">
                                    <label class="col-sm-3 control-label">Danger</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="panel-divider"></div>
                                <div class="form-group has-success has-feedback">
                                    <label class="col-sm-3 control-label">Success</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"><span class="zmdi zmdi-check form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group has-warning has-feedback">
                                    <label class="col-sm-3 control-label">Warning</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"><span class="zmdi zmdi-alert-triangle form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="form-group has-error has-feedback">
                                    <label class="col-sm-3 control-label">Danger</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control"><span class="zmdi zmdi-close form-control-feedback"></span>
                                    </div>
                                </div>
                                <div class="panel-divider"></div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Checkbox</label>
                                    <div class="col-sm-6">
                                        <div class="ai-checkbox has-success">
                                            <input id="checksucc" type="checkbox" checked="">
                                            <label for="checksucc">Success</label>
                                        </div>
                                        <div class="ai-checkbox has-warning">
                                            <input id="checkwarn" type="checkbox">
                                            <label for="checkwarn">Warning</label>
                                        </div>
                                        <div class="ai-checkbox has-danger">
                                            <input id="checkdang" type="checkbox">
                                            <label for="checkdang">Danger</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Full Color</label>
                                    <div class="col-sm-6">
                                        <div class="ai-checkbox ai-checkbox-color has-success inline">
                                            <input id="checkinsucc" type="checkbox" checked="">
                                            <label for="checkinsucc">Success</label>
                                        </div>
                                        <div class="ai-checkbox ai-checkbox-color has-warning inline">
                                            <input id="checkinwarn" type="checkbox" checked="">
                                            <label for="checkinwarn">Warning</label>
                                        </div>
                                        <div class="ai-checkbox ai-checkbox-color has-danger inline">
                                            <input id="checkindang" type="checkbox" checked="">
                                            <label for="checkindang">Danger</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Radio</label>
                                    <div class="col-sm-6">
                                        <div class="ai-radio has-success">
                                            <input type="radio" checked="" name="rad2" id="radsucc">
                                            <label for="radsucc">Success</label>
                                        </div>
                                        <div class="ai-radio has-warning">
                                            <input type="radio" name="rad2" id="radwarn">
                                            <label for="radwarn">Warning</label>
                                        </div>
                                        <div class="ai-radio has-danger">
                                            <input type="radio" name="rad2" id="raddang">
                                            <label for="raddang">Danger</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Inline Radio</label>
                                    <div class="col-sm-6">
                                        <div class="ai-radio has-success inline">
                                            <input type="radio" checked="" name="rad3" id="radinsucc">
                                            <label for="radinsucc">Success</label>
                                        </div>
                                        <div class="ai-radio has-warning inline">
                                            <input type="radio" name="rad3" id="radinwarn">
                                            <label for="radinwarn">Warning</label>
                                        </div>
                                        <div class="ai-radio has-danger inline">
                                            <input type="radio" name="rad3" id="radindang">
                                            <label for="radindang">Danger</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Full Color</label>
                                    <div class="col-sm-6">
                                        <div class="ai-radio ai-radio-color has-success inline">
                                            <input type="radio" checked="" name="rad4" id="rad34">
                                            <label for="rad34">Success</label>
                                        </div>
                                        <div class="ai-radio ai-radio-color has-warning inline">
                                            <input type="radio" name="rad4" id="rad35">
                                            <label for="rad35">Warning</label>
                                        </div>
                                        <div class="ai-radio ai-radio-color has-danger inline">
                                            <input type="radio" name="rad4" id="rad36">
                                            <label for="rad36">Danger</label>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Input Groups-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Input Groups<span class="panel-subtitle">Bootstrap input groups components</span></div>
                        <div class="panel-body">
                            <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Input Text</label>
                                    <div class="col-sm-6">
                                        <div class="input-group xs-mb-15"><span class="input-group-addon">@</span>
                                            <input type="text" placeholder="Username" class="form-control">
                                        </div>
                                        <div class="input-group xs-mb-15">
                                            <input type="text" class="form-control"><span class="input-group-addon">.00</span>
                                        </div>
                                        <div class="input-group xs-mb-15"><span class="input-group-addon">$</span>
                                            <input type="text" class="form-control"><span class="input-group-addon">.00</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Input Sizing</label>
                                    <div class="col-sm-6">
                                        <div class="input-group input-group-lg xs-mb-15"><span class="input-group-addon">@</span>
                                            <input type="text" placeholder="Username" class="form-control">
                                        </div>
                                        <div class="input-group xs-mb-15"><span class="input-group-addon">@</span>
                                            <input type="text" placeholder="Username" class="form-control">
                                        </div>
                                        <div class="input-group input-group-sm xs-mb-15"><span class="input-group-addon">@</span>
                                            <input type="text" placeholder="Username" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Checkbox & Radio</label>
                                    <div class="col-sm-6">
                                        <div class="input-group xs-mb-15">
                                            <div class="input-group-addon">
                                                <div class="ai-checkbox">
                                                    <input type="checkbox" id="check12">
                                                    <label for="check12"></label>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="input-group xs-mb-15">
                                            <div class="input-group-addon">
                                                <div class="ai-radio">
                                                    <input type="radio" id="rad12">
                                                    <label for="rad12"></label>
                                                </div>
                                            </div>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Button Addons</label>
                                    <div class="col-sm-6">
                                        <div class="input-group xs-mb-15">
                                            <input type="text" class="form-control"><span class="input-group-btn">
                                            <button type="button" class="btn btn-primary">Go!</button></span>
                                        </div>
                                        <div class="input-group xs-mb-15">
                                            <input type="text" class="form-control">
                                            <div class="input-group-btn">
                                                <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Action <span class="caret"></span></button>
                                                <ul class="dropdown-menu pull-right">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="input-group xs-mb-15">
                                            <input type="text" class="form-control">
                                            <div class="input-group-btn">
                                                <button tabindex="-1" type="button" class="btn btn-primary">Action</button>
                                                <button tabindex="-1" data-toggle="dropdown" type="button" class="btn btn-primary btn-shade1 dropdown-toggle"><span class="caret"></span><span class="sr-only">Toggle Dropdown</span></button>
                                                <ul role="menu" class="dropdown-menu pull-right">
                                                    <li><a href="#">Action</a></li>
                                                    <li><a href="#">Another action</a></li>
                                                    <li><a href="#">Something else here</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="#">Separated link</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Swtich Component-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Switch Component<span class="panel-subtitle">Custom switch component using only css</span></div>
                        <div class="panel-body">
                            <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Sizes</label>
                                    <div class="col-sm-6">
                                        <div class="switch-button switch-button-xs">
                                            <input type="checkbox" checked="" name="swt1" id="swt1"><span>
                                            <label for="swt1"></label></span>
                                        </div>
                                        <div class="switch-button switch-button-sm">
                                            <input type="checkbox" checked="" name="swt2" id="swt2"><span>
                                            <label for="swt2"></label></span>
                                        </div>
                                        <div class="switch-button">
                                            <input type="checkbox" checked="" name="swt3" id="swt3"><span>
                                            <label for="swt3"></label></span>
                                        </div>
                                        <div class="switch-button switch-button-lg">
                                            <input type="checkbox" checked="" name="swt4" id="swt4"><span>
                                            <label for="swt4"></label></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Success</label>
                                    <div class="col-sm-6 xs-pt-5">
                                        <div class="switch-button switch-button-success">
                                            <input type="checkbox" checked="" name="swt5" id="swt5"><span>
                                            <label for="swt5"></label></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Warning</label>
                                    <div class="col-sm-6 xs-pt-5">
                                        <div class="switch-button switch-button-warning">
                                            <input type="checkbox" checked="" name="swt6" id="swt6"><span>
                                            <label for="swt6"></label></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Danger</label>
                                    <div class="col-sm-6 xs-pt-5">
                                        <div class="switch-button switch-button-danger">
                                            <input type="checkbox" checked="" name="swt7" id="swt7"><span>
                                            <label for="swt7"></label></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Yes/No Labels</label>
                                    <div class="col-sm-6 xs-pt-5">
                                        <div class="switch-button switch-button-yesno">
                                            <input type="checkbox" checked="" name="swt8" id="swt8"><span>
                                            <label for="swt8"></label></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Datepicker-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Date Picker<span class="panel-subtitle">Datepicker bootstrap plugin</span></div>
                        <div class="panel-body">
                            <form action="#" style="border-radius: 0px;" class="form-horizontal group-border-dashed">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Default</label>
                                    <div class="col-sm-6">
                                        <input type="text" value="" class="form-control datetimepicker">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Read Only</label>
                                    <div class="col-md-3 col-xs-7">
                                        <div data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd - HH:ii" data-link-field="dtp_input1" class="input-group date datetimepicker">
                                            <input size="16" type="text" value="" readonly="" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th zmdi zmdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"> Only Date </label>
                                    <div class="col-md-3 col-xs-7">
                                        <div data-min-view="2" data-date-format="yyyy-mm-dd" class="input-group date datetimepicker">
                                            <input size="16" type="text" value="" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th zmdi zmdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Decade View</label>
                                    <div class="col-md-3 col-xs-7">
                                        <div data-start-view="4" data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd - HH:ii" data-link-field="dtp_input1" class="input-group date datetimepicker">
                                            <input size="16" type="text" value="" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th zmdi zmdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Year View</label>
                                    <div class="col-md-3 col-xs-7">
                                        <div data-start-view="3" data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd - HH:ii" data-link-field="dtp_input1" class="input-group date datetimepicker">
                                            <input size="16" type="text" value="" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th zmdi zmdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Month View</label>
                                    <div class="col-md-3 col-xs-7">
                                        <div data-start-view="2" data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd - HH:ii" data-link-field="dtp_input1" class="input-group date datetimepicker">
                                            <input size="16" type="text" value="" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th zmdi zmdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Day View</label>
                                    <div class="col-md-3 col-xs-7">
                                        <div data-start-view="1" data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd - HH:ii" data-link-field="dtp_input1" class="input-group date datetimepicker">
                                            <input size="16" type="text" value="" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th zmdi zmdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Hour View</label>
                                    <div class="col-md-3 col-xs-7">
                                        <div data-start-view="0" data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd - HH:ii" data-link-field="dtp_input1" class="input-group date datetimepicker">
                                            <input size="16" type="text" value="" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th zmdi zmdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Day View Meridian</label>
                                    <div class="col-md-3 col-xs-7">
                                        <div data-show-meridian="true" data-start-view="1" data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd - HH:ii" data-link-field="dtp_input1" class="input-group date datetimepicker">
                                            <input size="16" type="text" value="" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th zmdi zmdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Hour View Meridian</label>
                                    <div class="col-md-3 col-xs-7">
                                        <div data-show-meridian="true" data-start-view="0" data-date="1979-09-16T05:25:07Z" data-date-format="yyyy-mm-dd - HH:ii" data-link-field="dtp_input1" class="input-group date datetimepicker">
                                            <input size="16" type="text" value="" class="form-control"><span class="input-group-addon btn btn-primary"><i class="icon-th zmdi zmdi-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!--Select2 & Slider-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default panel-border-color panel-border-color-primary">
                        <div class="panel-heading panel-heading-divider">Advanced Controls<span class="panel-subtitle">Select2 & Bootstrap slider plugins</span></div>
                        <div class="panel-body">
                            <form action="#" class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Select2</label>
                                    <div class="col-sm-6">
                                        <select class="select2">
                                            <optgroup label="Alaskan/Hawaiian Time Zone">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                            </optgroup>
                                            <optgroup label="Pacific Time Zone">
                                                <option value="CA">California</option>
                                                <option value="NV">Nevada</option>
                                                <option value="OR">Oregon</option>
                                                <option value="WA">Washington</option>
                                            </optgroup>
                                            <optgroup label="Mountain Time Zone">
                                                <option value="AZ">Arizona</option>
                                                <option value="CO">Colorado</option>
                                                <option value="ID">Idaho</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="UT">Utah</option>
                                                <option value="WY">Wyoming</option>
                                            </optgroup>
                                            <optgroup label="Central Time Zone">
                                                <option value="AL">Alabama</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TX">Texas</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="WI">Wisconsin</option>
                                            </optgroup>
                                            <optgroup label="Eastern Time Zone">
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="IN">Indiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="OH">Ohio</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WV">West Virginia</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">MultiTag Input</label>
                                    <div class="col-sm-6">
                                        <select multiple="" class="tags">
                                            <option value="1">Green</option>
                                            <option value="2">Red</option>
                                            <option value="3">Blue</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Multitag from Select</label>
                                    <div class="col-sm-6">
                                        <select multiple="" class="select2">
                                            <optgroup label="Alaskan/Hawaiian Time Zone">
                                                <option value="AK">Alaska</option>
                                                <option value="HI">Hawaii</option>
                                            </optgroup>
                                            <optgroup label="Pacific Time Zone">
                                                <option value="CA">California</option>
                                                <option value="NV">Nevada</option>
                                                <option value="OR">Oregon</option>
                                                <option value="WA">Washington</option>
                                            </optgroup>
                                            <optgroup label="Mountain Time Zone">
                                                <option value="AZ">Arizona</option>
                                                <option value="CO">Colorado</option>
                                                <option value="ID">Idaho</option>
                                                <option value="MT">Montana</option>
                                                <option value="NE">Nebraska</option>
                                                <option value="NM">New Mexico</option>
                                                <option value="ND">North Dakota</option>
                                                <option value="UT">Utah</option>
                                                <option value="WY">Wyoming</option>
                                            </optgroup>
                                            <optgroup label="Central Time Zone">
                                                <option value="AL">Alabama</option>
                                                <option value="AR">Arkansas</option>
                                                <option value="IL">Illinois</option>
                                                <option value="IA">Iowa</option>
                                                <option value="KS">Kansas</option>
                                                <option value="KY">Kentucky</option>
                                                <option value="LA">Louisiana</option>
                                                <option value="MN">Minnesota</option>
                                                <option value="MS">Mississippi</option>
                                                <option value="MO">Missouri</option>
                                                <option value="OK">Oklahoma</option>
                                                <option value="SD">South Dakota</option>
                                                <option value="TX">Texas</option>
                                                <option value="TN">Tennessee</option>
                                                <option value="WI">Wisconsin</option>
                                            </optgroup>
                                            <optgroup label="Eastern Time Zone">
                                                <option value="CT">Connecticut</option>
                                                <option value="DE">Delaware</option>
                                                <option value="FL">Florida</option>
                                                <option value="GA">Georgia</option>
                                                <option value="IN">Indiana</option>
                                                <option value="ME">Maine</option>
                                                <option value="MD">Maryland</option>
                                                <option value="MA">Massachusetts</option>
                                                <option value="MI">Michigan</option>
                                                <option value="NH">New Hampshire</option>
                                                <option value="NJ">New Jersey</option>
                                                <option value="NY">New York</option>
                                                <option value="NC">North Carolina</option>
                                                <option value="OH">Ohio</option>
                                                <option value="PA">Pennsylvania</option>
                                                <option value="RI">Rhode Island</option>
                                                <option value="SC">South Carolina</option>
                                                <option value="VT">Vermont</option>
                                                <option value="VA">Virginia</option>
                                                <option value="WV">West Virginia</option>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Slider</label>
                                    <div class="col-sm-6">
                                        <input type="text" value="" class="bslider form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Range Slider</label>
                                    <div class="col-sm-6">
                                        <input type="text" data-slider-value="[250,450]" data-slider-step="5" data-slider-max="1000" data-slider-min="10" value="" class="bslider form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Vertical Slider</label>
                                    <div class="col-sm-6">
                                        <input type="text" data-slider-selection="after" data-slider-orientation="vertical" data-slider-value="-13" data-slider-step="1" data-slider-max="20" data-slider-min="-20" value="" class="form-control bslider">
                                        <input type="text" data-slider-selection="after" data-slider-orientation="vertical" data-slider-value="-9" data-slider-step="1" data-slider-max="20" data-slider-min="-20" value="" class="form-control bslider">
                                        <input type="text" data-slider-selection="after" data-slider-orientation="vertical" data-slider-value="-5" data-slider-step="1" data-slider-max="20" data-slider-min="-20" value="" class="form-control bslider">
                                        <input type="text" data-slider-selection="after" data-slider-orientation="vertical" data-slider-value="-2" data-slider-step="1" data-slider-max="20" data-slider-min="-20" value="" class="form-control bslider">
                                        <input type="text" data-slider-selection="after" data-slider-orientation="vertical" data-slider-value="-5" data-slider-step="1" data-slider-max="20" data-slider-min="-20" value="" class="form-control bslider">
                                        <input type="text" data-slider-selection="after" data-slider-orientation="vertical" data-slider-value="-9" data-slider-step="1" data-slider-max="20" data-slider-min="-20" value="" class="form-control bslider">
                                        <input type="text" data-slider-selection="after" data-slider-orientation="vertical" data-slider-value="-13" data-slider-step="1" data-slider-max="20" data-slider-min="-20" value="" class="form-control bslider">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection