<div class="ai-left-sidebar">
    <div class="ai-left-sidebar-container">
        <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Blank Page</a>
            <div class="left-sidebar-spacer">
                <div class="left-sidebar-scroll">
                    <div class="left-sidebar-content">
                        <ul class="sidebar-elements">
                            <li class="divider">Menu</li>
                            <li class="{{ Request::segment(1) == '' ? 'active' : '' }}">
                                <a href="/">
                                    <i class="icon zmdi zmdi-home"></i><span>Home</span>
                                </a>
                            </li>
                            <li class="parent"><a href="#"><i class="icon zmdi zmdi-view-dashboard"></i><span>Elements</span></a>
                                <ul class="sub-menu">
                                    <li class="{{ Request::segment(1) == 'element-alert' ? 'active' : '' }}">
                                        <a href="/element-alert">Alert</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'element-button' ? 'active' : '' }}">
                                        <a href="/element-button">Button</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'element-panel' ? 'active' : '' }}">
                                        <a href="/element-panel">Panel</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'element-modal' ? 'active' : '' }}">
                                        <a href="/element-modal">Modal</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'element-notification' ? 'active' : '' }}">
                                        <a href="/element-notification">Notification</a>
                                    </li>

                                    <li class="{{ Request::segment(1) == 'element-icon' ? 'active' : '' }}">
                                        <a href="http://zavoloklom.github.io/material-design-iconic-font/icons.html" target="_blank">Icon</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'element-tab' ? 'active' : '' }}">
                                        <a href="/element-tab">Tab & Accordions</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="parent"><a href="#"><i class="icon zmdi zmdi-dot-circle"></i><span>Form</span></a>
                                <ul class="sub-menu">
                                    <li class="{{ Request::segment(1) == 'form-general' ? 'active' : '' }}">
                                        <a href="/form-general">General</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'form-mask' ? 'active' : '' }}">
                                        <a href="/form-mask">Mask</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'form-tinymce' ? 'active' : '' }}">
                                        <a href="/form-tinymce">Tinymce</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'form-multipleupload' ? 'active' : '' }}">
                                        <a href="/form-multipleupload">Multiple Upload</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="parent"><a href="#"><i class="icon zmdi zmdi-grid"></i><span>Table</span></a>
                                <ul class="sub-menu">
                                    <li class="{{ Request::segment(1) == 'table-general' ? 'active' : '' }}">
                                        <a href="/table-general">General</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'table-datatables' ? 'active' : '' }}">
                                        <a href="/table-datatables">Datatables</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="parent"><a href="#"><i class="icon zmdi zmdi-layers"></i><span>Page</span></a>
                                <ul class="sub-menu">
                                    <li class="{{ Request::segment(1) == '404' ? 'active' : '' }}">
                                        <a href="/404">404</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'login' ? 'active' : '' }}">
                                        <a href="/login">Login</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'register' ? 'active' : '' }}">
                                        <a href="/register">Register</a>
                                    </li>
                                    <li class="{{ Request::segment(1) == 'forgot-password' ? 'active' : '' }}">
                                        <a href="/forgot-password">Forgot Password</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>