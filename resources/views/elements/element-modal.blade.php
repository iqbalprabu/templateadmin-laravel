@extends('section.app')
@section('content')
    <div class="ai-content">
        <div class="page-head">
            <h2 class="page-head-title">Modals</h2>
        </div>
        <div class="main-content container-fluid">
            <div class="row">
                <!--Modal Alerts-->
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading-divider">Modal Alerts<span class="panel-subtitle">Default bootstrap modal component</span></div>
                        <div class="panel-body">
                            <p>Modals are streamlined, but flexible, dialog prompts with the minimum required functionality and smart defaults.</p>
                            <div class="xs-mt-30 xs-mb-20 text-center">
                                <button data-toggle="modal" data-target="#mod-success" type="button" class="btn btn-space btn-success">Success</button>
                                <button data-toggle="modal" data-target="#mod-primary" type="button" class="btn btn-space btn-primary">Primary</button>
                                <button data-toggle="modal" data-target="#mod-warning" type="button" class="btn btn-space btn-warning">Warning</button>
                                <button data-toggle="modal" data-target="#mod-danger" type="button" class="btn btn-space btn-danger">Danger</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Modal Footer-->
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading-divider">Modal Footer<span class="panel-subtitle">Default bootstrap modal with a footer content</span></div>
                        <div class="panel-body">
                            <p>Modals are streamlined, but flexible, dialog prompts with the minimum required functionality and smart defaults.</p>
                            <div class="xs-mt-30 xs-mb-20 text-center">
                                <button data-toggle="modal" data-target="#md-footer-success" type="button" class="btn btn-space btn-success">Success</button>
                                <button data-toggle="modal" data-target="#md-footer-primary" type="button" class="btn btn-space btn-primary">Primary</button>
                                <button data-toggle="modal" data-target="#md-footer-warning" type="button" class="btn btn-space btn-warning">Warning</button>
                                <button data-toggle="modal" data-target="#md-footer-danger" type="button" class="btn btn-space btn-danger">Danger</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--Full Color Alerts-->
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading-divider">Full Color Alerts<span class="panel-subtitle">Nifty modals with full color background style</span></div>
                        <div class="panel-body">
                            <p>Modals are streamlined, but flexible, dialog prompts with the minimum required functionality and smart defaults.</p>
                            <div class="xs-mt-30 xs-mb-20 text-center">
                                <button data-modal="full-success" class="btn btn-space btn-success md-trigger">Success</button>
                                <button data-modal="full-primary" class="btn btn-space btn-primary md-trigger">Primary</button>
                                <button data-modal="full-warning" class="btn btn-space btn-warning md-trigger">Warning</button>
                                <button data-modal="full-danger" class="btn btn-space btn-danger md-trigger">Danger</button>
                                <button data-modal="full-dark" class="btn btn-space btn-default md-trigger">Dark</button>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="full-success" class="modal-container modal-full-color modal-full-color-success modal-effect-8">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center"><span class="modal-main-icon zmdi zmdi-check"></span>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-success btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="full-primary" class="modal-container modal-full-color modal-full-color-primary modal-effect-8">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center"><span class="modal-main-icon zmdi zmdi-info-outline"></span>
                                            <h3>Information!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-success btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="full-warning" class="modal-container modal-full-color modal-full-color-warning modal-effect-8">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center"><span class="modal-main-icon zmdi zmdi-alert-triangle"></span>
                                            <h3>Warning!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-success btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="full-danger" class="modal-container modal-full-color modal-full-color-danger modal-effect-8">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center"><span class="modal-main-icon zmdi zmdi-close-circle-o"></span>
                                            <h3>Danger!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-success btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="full-dark" class="modal-container modal-full-color modal-full-color-dark modal-effect-8">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center"><span class="modal-main-icon zmdi zmdi-close-circle-o"></span>
                                            <h3>Danger!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-success btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <div class="modal-overlay"></div>
                        </div>
                    </div>
                </div>
                <!--Colored Header Modals-->
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading-divider">Colored Header Modal<span class="panel-subtitle">Nifty modals with colored header style</span></div>
                        <div class="panel-body">
                            <p>Modals are streamlined, but flexible, dialog prompts with the minimum required functionality and smart defaults.</p>
                            <div class="xs-mt-30 xs-mb-20 text-center">
                                <button data-modal="colored-success" class="btn btn-space btn-success md-trigger">Success</button>
                                <button data-modal="colored-primary" class="btn btn-space btn-primary md-trigger">Primary</button>
                                <button data-modal="colored-warning" class="btn btn-space btn-warning md-trigger">Warning</button>
                                <button data-modal="colored-danger" class="btn btn-space btn-danger md-trigger">Danger</button>
                                <button data-modal="colored-dark" class="btn btn-space btn-default md-trigger">Dark</button>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="colored-success" class="modal-container colored-header colored-header-success modal-effect-10">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                        <h3 class="modal-title">Custom Header Color</h3>
                                    </div>
                                    <div class="modal-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.</p>
                                        <p>Fusce sit amet lacus arcu. Pellentesque iaculis massa vitae ullamcorper venenatis. Cras metus dolor, maximus sit amet est nec, pharetra blandit elit sagittis vehicula.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-success modal-close">Proceed</button>
                                    </div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="colored-primary" class="modal-container colored-header colored-header-primary modal-effect-10">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                        <h3 class="modal-title">Custom Header Color</h3>
                                    </div>
                                    <div class="modal-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.</p>
                                        <p>Fusce sit amet lacus arcu. Pellentesque iaculis massa vitae ullamcorper venenatis. Cras metus dolor, maximus sit amet est nec, pharetra blandit elit sagittis vehicula.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-primary modal-close">Proceed</button>
                                    </div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="colored-warning" class="modal-container colored-header colored-header-warning modal-effect-10">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                        <h3 class="modal-title">Custom Header Color</h3>
                                    </div>
                                    <div class="modal-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.</p>
                                        <p>Fusce sit amet lacus arcu. Pellentesque iaculis massa vitae ullamcorper venenatis. Cras metus dolor, maximus sit amet est nec, pharetra blandit elit sagittis vehicula.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-warning modal-close">Proceed</button>
                                    </div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="colored-danger" class="modal-container colored-header colored-header-danger modal-effect-10">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                        <h3 class="modal-title">Custom Header Color</h3>
                                    </div>
                                    <div class="modal-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.</p>
                                        <p>Fusce sit amet lacus arcu. Pellentesque iaculis massa vitae ullamcorper venenatis. Cras metus dolor, maximus sit amet est nec, pharetra blandit elit sagittis vehicula.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-danger modal-close">Proceed</button>
                                    </div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="colored-dark" class="modal-container colored-header colored-header-dark modal-effect-10">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                        <h3 class="modal-title">Custom Header Color</h3>
                                    </div>
                                    <div class="modal-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.</p>
                                        <p>Fusce sit amet lacus arcu. Pellentesque iaculis massa vitae ullamcorper venenatis. Cras metus dolor, maximus sit amet est nec, pharetra blandit elit sagittis vehicula.</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Proceed</button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-overlay"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--Bootstrap Modals-->
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading-divider">Bootstrap Modals<span class="panel-subtitle">These are the different modal styles using bootstrap modals</span></div>
                        <div class="panel-body">
                            <p>Modals are streamlined, but flexible, dialog prompts with the minimum required functionality and smart defaults.</p>
                            <div class="xs-mt-30 xs-mb-20 text-center">
                                <button data-toggle="modal" data-target="#md-default" type="button" class="btn btn-space btn-primary"> Default</button>
                                <button data-toggle="modal" data-target="#md-fullWidth" type="button" class="btn btn-space btn-primary"> Full Width</button>
                                <button data-toggle="modal" data-target="#md-custom" type="button" class="btn btn-space btn-primary"> Custom width</button>
                                <button data-toggle="modal" data-target="#md-colored" type="button" class="btn btn-space btn-primary"> Colored Header</button>
                                <button data-toggle="modal" data-target="#md-fullColor" type="button" class="btn btn-space btn-primary"> Full Color</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Nifty Modals  -->
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading-divider">Nifty Modals<span class="panel-subtitle">These are the different modal styles using nifty modals</span></div>
                        <div class="panel-body">
                            <p>Modals are streamlined, but flexible, dialog prompts with the minimum required functionality and smart defaults.</p>
                            <div class="xs-mt-30 xs-mb-20 text-center">
                                <button data-modal="nft-default" class="btn btn-space btn-success md-trigger"> Default</button>
                                <button data-modal="nft-fullWidth" class="btn btn-space btn-success md-trigger"> Full Width</button>
                                <button data-modal="nft-custom" class="btn btn-space btn-success md-trigger"> Custom width</button>
                                <button data-modal="nft-colored" class="btn btn-space btn-success md-trigger"> Colored Header</button>
                                <button data-modal="nft-fullColor" class="btn btn-space btn-success md-trigger"> Full Color</button>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="nft-default" class="modal-container modal-effect-1">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-success"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-success btn-space">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="nft-fullWidth" class="modal-container full-width modal-effect-1">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-success"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-success btn-space">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="nft-custom" class="modal-container modal-effect-1">
                                <div style="width: 700px;" class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-success"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-success btn-space">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="nft-colored" class="modal-container colored-header colored-header-success modal-effect-1">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                        <h3 class="modal-title">Custom Header Color</h3>
                                    </div>
                                    <div class="modal-body">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.</p>
                                        <p>Fusce sit amet lacus arcu. Pellentesque iaculis massa vitae ullamcorper venenatis. Cras metus dolor, maximus sit amet est nec, pharetra blandit elit sagittis vehicula. </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-success modal-close">Proceed</button>
                                    </div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="nft-fullColor" class="modal-container modal-full-color modal-full-color-success modal-effect-1">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center"><span class="modal-main-icon zmdi zmdi-check"></span>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-success btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <div class="modal-overlay"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--Nifty Modals Effects-->
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading-divider">Nifty Modals Effects<span class="panel-subtitle">These are examples of niftymodals effects</span></div>
                        <div class="panel-body">
                            <p>jQuery NiftyModal plugin has 18 different amazing effects to give your modals life.</p>
                            <div class="xs-mt-30 xs-mb-20 text-center">
                                <button data-modal="md-scale" class="btn btn-space btn-primary md-trigger"> Fade in & Scale</button>
                                <button data-modal="md-slideLeft" class="btn btn-space btn-primary md-trigger"> Slide in Left</button>
                                <button data-modal="md-slideUp" class="btn btn-space btn-primary md-trigger"> Slide in Up</button>
                                <button data-modal="md-fall" class="btn btn-space btn-primary md-trigger"> Fall</button>
                                <button data-modal="md-sideFall" class="btn btn-space btn-primary md-trigger"> Side Fall</button>
                                <button data-modal="md-stickyUp" class="btn btn-space btn-primary md-trigger"> Sticky Up</button>
                                <button data-modal="md-flipH" class="btn btn-space btn-primary md-trigger"> 3D Flip Horizontal</button>
                                <button data-modal="md-flipV" class="btn btn-space btn-primary md-trigger"> 3D Flip Vertical</button>
                                <button data-modal="md-3dSign" class="btn btn-space btn-primary md-trigger"> 3D Sign</button>
                                <button data-modal="md-superScaled" class="btn btn-space btn-primary md-trigger"> Super Scaled</button>
                                <button data-modal="md-justModal" class="btn btn-space btn-primary md-trigger"> Just Modal</button>
                                <button data-modal="md-3dRotateUp" class="btn btn-space btn-primary md-trigger"> 3D Rotate Up</button>
                                <button data-modal="md-3dRotateRight" class="btn btn-space btn-primary md-trigger"> 3D Rotate Right</button>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-scale" class="modal-container modal-effect-1">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-slideLeft" class="modal-container modal-effect-2">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-slideUp" class="modal-container modal-effect-3">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-fall" class="modal-container modal-effect-5">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-sideFall" class="modal-container modal-effect-6">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-stickyUp" class="modal-container modal-effect-7">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-flipH" class="modal-container modal-effect-8">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-flipV" class="modal-container modal-effect-9">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-3dSign" class="modal-container modal-effect-10">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-superScaled" class="modal-container modal-effect-11">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-justModal" class="modal-container modal-effect-12">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-3dRotateUp" class="modal-container modal-effect-14">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="md-3dRotateRight" class="modal-container modal-effect-15">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="text-center">
                                            <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                                            <h3>Awesome!</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                                            <div class="xs-mt-50">
                                                <button type="button" data-dismiss="modal" class="btn btn-default btn-space modal-close">Cancel</button>
                                                <button type="button" data-dismiss="modal" class="btn btn-primary btn-space modal-close">Proceed</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer"></div>
                                </div>
                            </div>
                            <div class="modal-overlay"></div>
                        </div>
                    </div>
                </div>
                <!--Form Modals-->
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading panel-heading-divider">Form Modals<span class="panel-subtitle">These are examples of forms inside modals</span></div>
                        <div class="panel-body">
                            <p>Modals are streamlined, but flexible, dialog prompts with the minimum required functionality and smart defaults.</p>
                            <div class="xs-mt-30 xs-mb-20 text-center">
                                <button data-modal="form-success" class="btn btn-space btn-success md-trigger">Nifty Modal</button>
                                <button data-toggle="modal" data-target="#form-bp1" type="button" class="btn btn-space btn-primary">Bootstrap Modal</button>
                            </div>
                            <!-- Nifty Modal-->
                            <div id="form-success" class="modal-container colored-header colored-header-success custom-width modal-effect-9">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close modal-close"><span class="zmdi zmdi-close"></span></button>
                                        <h3 class="modal-title">Nifty Form Modal</h3>
                                    </div>
                                    <div class="modal-body form">
                                        <div class="form-group">
                                            <label>Email address</label>
                                            <input type="email" placeholder="username@example.com" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Your name</label>
                                            <input type="text" placeholder="John Doe" class="form-control">
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label>Your birth date</label>
                                            </div>
                                        </div>
                                        <div class="row no-margin-y">
                                            <div class="form-group col-xs-3">
                                                <input type="text" placeholder="DD" class="form-control">
                                            </div>
                                            <div class="form-group col-xs-3">
                                                <input type="text" placeholder="MM" class="form-control">
                                            </div>
                                            <div class="form-group col-xs-3">
                                                <input type="text" placeholder="YYYY" class="form-control">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <div class="ai-checkbox">
                                                    <input id="check1" type="checkbox">
                                                    <label for="check1">Send me notifications about new products and services.</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" data-dismiss="modal" class="btn btn-default modal-close">Cancel</button>
                                        <button type="button" data-dismiss="modal" class="btn btn-success modal-close">Proceed</button>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-overlay"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Modal Alerts-->
    <div id="mod-success" tabindex="-1" role="dialog" style="" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-success"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                        <h3>Awesome!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                        <div class="xs-mt-50">
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-success">Proceed</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <div id="mod-primary" tabindex="-1" role="dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-primary"><span class="modal-main-icon zmdi zmdi-info-outline"></span></div>
                        <h3>Information!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                        <div class="xs-mt-50">
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-primary">Proceed</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <div id="mod-warning" tabindex="-1" role="dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-warning"><span class="modal-main-icon zmdi zmdi-alert-triangle"></span></div>
                        <h3>Warning!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                        <div class="xs-mt-50">
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-warning">Proceed</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <div id="mod-danger" tabindex="-1" role="dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-danger"><span class="modal-main-icon zmdi zmdi-close-circle-o"></span></div>
                        <h3>Danger!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                        <div class="xs-mt-50">
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-danger">Proceed</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <!--Modal Footer-->
    <div id="md-footer-success" tabindex="-1" role="dialog" style="" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-success"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                        <h3>Awesome!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-success">Proceed</button>
                </div>
            </div>
        </div>
    </div>
    <div id="md-footer-primary" tabindex="-1" role="dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-primary"><span class="modal-main-icon zmdi zmdi-info-outline"></span></div>
                        <h3>Information!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-primary">Proceed</button>
                </div>
            </div>
        </div>
    </div>
    <div id="md-footer-warning" tabindex="-1" role="dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-warning"><span class="modal-main-icon zmdi zmdi-alert-triangle"></span></div>
                        <h3>Warning!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-warning">Proceed</button>
                </div>
            </div>
        </div>
    </div>
    <div id="md-footer-danger" tabindex="-1" role="dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-danger"><span class="modal-main-icon zmdi zmdi-close-circle-o"></span></div>
                        <h3>Danger!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-danger">Proceed</button>
                </div>
            </div>
        </div>
    </div>
    <!--Bootstrao Modals-->
    <div id="md-default" tabindex="-1" role="dialog" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                        <h3>Awesome!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                        <div class="xs-mt-50">
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-primary">Proceed</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <div id="md-fullWidth" tabindex="-1" role="dialog" class="modal fade">
        <div class="modal-dialog full-width">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                        <h3>Awesome!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                        <div class="xs-mt-50">
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-primary">Proceed</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <div id="md-custom" tabindex="-1" role="dialog" class="modal fade">
        <div style="width: 700px;" class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center">
                        <div class="text-primary"><span class="modal-main-icon zmdi zmdi-check"></span></div>
                        <h3>Awesome!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                        <div class="xs-mt-50">
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-default">Cancel</button>
                            <button type="button" data-dismiss="modal" class="btn btn-space btn-primary">Proceed</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <div id="md-colored" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                    <h3 class="modal-title">Custom Colored Header</h3>
                </div>
                <div class="modal-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ultrices euismod lobortis. Donec fermentum mattis velit id pretium. Suspendisse sed tortor sed diam lobortis tempus at sed lacus. Phasellus semper ex auctor libero scelerisque ultricies.</p>
                    <p>Fusce sit amet lacus arcu. Pellentesque iaculis massa vitae ullamcorper venenatis. Cras metus dolor, maximus sit amet est nec, pharetra blandit elit sagittis vehicula. </p>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-primary">Proceed</button>
                </div>
            </div>
        </div>
    </div>
    <div id="md-fullColor" tabindex="-1" role="dialog" class="modal modal-full-color modal-full-color-primary fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close"><span class="zmdi zmdi-close"></span></button>
                </div>
                <div class="modal-body">
                    <div class="text-center"><span class="modal-main-icon zmdi zmdi-info-outline"></span>
                        <h3>Information!</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.<br>Fusce ultrices euismod lobortis.</p>
                        <div class="xs-mt-50">
                            <button type="button" data-dismiss="modal" class="btn btn-default btn-space">Cancel</button>
                            <button type="button" data-dismiss="modal" class="btn btn-primary btn-space">Proceed</button>
                        </div>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>
    <!--Form Modals-->
    <div id="form-bp1" tabindex="-1" role="dialog" class="modal fade colored-header colored-header-primary">
        <div class="modal-dialog custom-width">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close md-close"><span class="zmdi zmdi-close"></span></button>
                    <h3 class="modal-title">Form Modal</h3>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" placeholder="username@example.com" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Your name</label>
                        <input type="text" placeholder="John Doe" class="form-control">
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Your birth date</label>
                        </div>
                    </div>
                    <div class="row no-margin-y">
                        <div class="form-group col-xs-3">
                            <input type="text" placeholder="DD" class="form-control">
                        </div>
                        <div class="form-group col-xs-3">
                            <input type="text" placeholder="MM" class="form-control">
                        </div>
                        <div class="form-group col-xs-3">
                            <input type="text" placeholder="YYYY" class="form-control">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="ai-checkbox">
                                <input id="check2" type="checkbox">
                                <label for="check2">Send me notifications about new products and services.</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-default md-close">Cancel</button>
                    <button type="button" data-dismiss="modal" class="btn btn-primary md-close">Proceed</button>
                </div>
            </div>
        </div>
    </div>
@endsection