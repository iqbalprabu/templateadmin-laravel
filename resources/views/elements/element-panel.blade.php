@extends('section.app')
@section('content')
    <div class="ai-content">
    <div class="page-head">
        <h2 class="page-head-title">Panels</h2>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-flat">
                    <div class="panel-heading">Flat Panel</div>
                    <div class="panel-body">
                        <p>Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio.</p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="tools"><span class="icon s7-upload"></span><span class="icon s7-edit"></span><span class="icon s7-close"></span></div><span class="title">Default Panel</span>
                    </div>
                    <div class="panel-body">
                        <p>Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio.</p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-border">
                    <div class="panel-heading"><span class="title">Panel with border</span></div>
                    <div class="panel-body">
                        <p>Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio.</p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel">
                    <div class="panel-heading panel-heading-divider">Panel with subtitle<span class="panel-subtitle">Panel subtitle description</span></div>
                    <div class="panel-body">
                        <p>Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio.</p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Dismissable panel
                        <div class="tools"><span class="icon zmdi zmdi-close"></span></div><span class="panel-subtitle">Panel subtitle description</span>
                    </div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio.</p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-border">
                    <div class="panel-heading panel-heading-divider">Panel with tools
                        <div class="tools"><span class="icon zmdi zmdi-refresh-sync"></span><span class="icon zmdi zmdi-more-vert"></span></div><span class="panel-subtitle">Panel subtitle description</span>
                    </div>
                    <div class="panel-body">
                        <p>Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio.</p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-contrast">
                    <div class="panel-heading panel-heading-contrast">Heading contrast<span class="panel-subtitle">Panel subtitle description</span></div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default panel-contrast">
                    <div class="panel-heading">Body contrast
                        <div class="tools"><span class="icon zmdi zmdi-close"></span></div><span class="panel-subtitle">Panel subtitle description</span>
                    </div>
                    <div class="panel-body panel-body-contrast">
                        <p>Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio.</p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.	</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-border panel-contrast">
                    <div class="panel-heading panel-heading-contrast">Panel with body contrast
                        <div class="tools"><span class="icon zmdi zmdi-refresh-sync"></span><span class="icon zmdi zmdi-close"></span></div><span class="panel-subtitle">Panel subtitle description</span>
                    </div>
                    <div class="panel-body">
                        <p>Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio.</p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-border-color panel-border-color-primary">
                    <div class="panel-heading">Border primary</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-border-color panel-border-color-danger">
                    <div class="panel-heading">Border danger</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-border-color panel-border-color-warning">
                    <div class="panel-heading">Border warning</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-border-color panel-border-color-success">
                    <div class="panel-heading">Border success</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-border-color">
                    <div class="panel-heading">Border default</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-border-color panel-border-color-dark">
                    <div class="panel-heading">Border dark</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-full-primary">
                    <div class="panel-heading">Primary Panel</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-full-danger">
                    <div class="panel-heading">Danger Panel</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-full-warning">
                    <div class="panel-heading">Warning Panel</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-full-success">
                    <div class="panel-heading">Success Panel</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-full">
                    <div class="panel-heading">Default Full Panel</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-full-dark">
                    <div class="panel-heading">Dark Panel</div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-full-color panel-full-primary">
                    <div class="panel-heading panel-heading-contrast">Success Panel
                        <div class="tools"><span class="icon zmdi zmdi-close"></span></div><span class="panel-subtitle">Panel subtitle description</span>
                    </div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-full-color panel-full-danger">
                    <div class="panel-heading panel-heading-contrast">Default Full Panel
                        <div class="tools"><span class="icon zmdi zmdi-close"></span></div><span class="panel-subtitle">Panel subtitle description</span>
                    </div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-full-color panel-full-warning">
                    <div class="panel-heading panel-heading-contrast">Dark Panel
                        <div class="tools"><span class="icon zmdi zmdi-close"></span></div><span class="panel-subtitle">Panel subtitle description</span>
                    </div>
                    <div class="panel-body">
                        <p> Quisque gravida aliquam diam at cursus, quisque laoreet ac lectus a rhoncusac tempus odio. </p>
                        <p>Aliquam posuere volutpat turpis, ut euimod diam pellentesque at. Sed sit amet nulla a dui dignisim euismod. Morbi luctus elementum dictum. Donec convallis mattis elit id varius. Quisque facilisis sapien quis mauris,, erat condimentum.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection