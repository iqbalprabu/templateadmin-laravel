@extends('section.app')
@section('content')
    <div class="ai-content">
    <div class="page-head">
        <h2 class="page-head-title">Buttons</h2>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Basic Buttons<span class="panel-subtitle">Default bootstrap buttons style</span></div>
                    <div class="panel-body xs-mt-15 text-center">
                        <p class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-default">Default</button>
                            <button class="btn btn-space btn-primary">Primary</button>
                            <button class="btn btn-space btn-danger">Danger</button>
                            <button class="btn btn-space btn-warning">Warning</button>
                            <button class="btn btn-space btn-success">Success</button>
                        </p>
                        <p class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-default hover">Default</button>
                            <button class="btn btn-space btn-primary hover">Primary</button>
                            <button class="btn btn-space btn-danger hover">Danger</button>
                            <button class="btn btn-space btn-warning hover">Warning</button>
                            <button class="btn btn-space btn-success hover">Success</button>
                        </p>
                        <p class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-default active">Default</button>
                            <button class="btn btn-space btn-primary active">Primary</button>
                            <button class="btn btn-space btn-danger active">Danger</button>
                            <button class="btn btn-space btn-warning active">Warning</button>
                            <button class="btn btn-space btn-success active">Success</button>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Button Icons<span class="panel-subtitle">Default bootstrap buttons with icons</span></div>
                    <div class="panel-body xs-mt-15 text-center">
                        <p class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-default"><i class="icon icon-left zmdi zmdi-globe"></i> Default</button>
                            <button class="btn btn-space btn-primary"><i class="icon icon-left zmdi zmdi-cloud-done"></i> Primary</button>
                            <button class="btn btn-space btn-danger"><i class="icon icon-left zmdi zmdi-alert-circle"></i> Danger</button>
                            <button class="btn btn-space btn-warning"><i class="icon icon-left zmdi zmdi-alert-triangle"></i> Warning</button>
                            <button class="btn btn-space btn-success"><i class="icon icon-left zmdi zmdi-check"></i> Success</button>
                        </p>
                        <p class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-default hover"> <i class="icon icon-left zmdi zmdi-globe"></i> Default</button>
                            <button class="btn btn-space btn-primary hover"><i class="icon icon-left zmdi zmdi-cloud-done"></i> Primary</button>
                            <button class="btn btn-space btn-danger hover"><i class="icon icon-left zmdi zmdi-alert-circle"></i> Danger</button>
                            <button class="btn btn-space btn-warning hover"><i class="icon icon-left zmdi zmdi-alert-triangle"></i> Warning</button>
                            <button class="btn btn-space btn-success hover"><i class="icon icon-left zmdi zmdi-check"></i> Success</button>
                        </p>
                        <p class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-default active"> <i class="icon icon-left zmdi zmdi-globe"></i> Default</button>
                            <button class="btn btn-space btn-primary active"><i class="icon icon-left zmdi zmdi-cloud-done"></i> Primary</button>
                            <button class="btn btn-space btn-danger active"><i class="icon icon-left zmdi zmdi-alert-circle"></i> Danger</button>
                            <button class="btn btn-space btn-warning active"><i class="icon icon-left zmdi zmdi-alert-triangle"></i> Warning</button>
                            <button class="btn btn-space btn-success active"><i class="icon icon-left zmdi zmdi-check"></i> Success</button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Buttons Size<span class="panel-subtitle">Different buttons size scale</span></div>
                    <div class="panel-body xs-mt-15">
                        <h4>Standard</h4>
                        <p class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-default btn-xs">Extra Small</button>
                            <button class="btn btn-space btn-default btn-sm">Small</button>
                            <button class="btn btn-space btn-default">Default</button>
                            <button class="btn btn-space btn-default btn-lg">Large</button>
                            <button class="btn btn-space btn-default btn-xl">Extra Large</button>
                        </p>
                        <h4 class="xs-mt-20">With Icons</h4>
                        <p class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-primary btn-xs"> <i class="icon icon-left zmdi zmdi-cloud-done"></i> Extra Small</button>
                            <button class="btn btn-space btn-primary btn-sm"><i class="icon icon-left zmdi zmdi-cloud-done"></i> Small</button>
                            <button class="btn btn-space btn-primary"><i class="icon icon-left zmdi zmdi-cloud-done"></i> Default</button>
                            <button class="btn btn-space btn-primary btn-lg"><i class="icon icon-left zmdi zmdi-cloud-done"></i> Large</button>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Buttons Rounded<span class="panel-subtitle">Buttons with rounded corners style</span></div>
                    <div class="panel-body xs-mt-15">
                        <h4>Standard</h4>
                        <p class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-rounded btn-space btn-default">Default</button>
                            <button class="btn btn-rounded btn-space btn-primary">Primary</button>
                            <button class="btn btn-rounded btn-space btn-danger">Danger</button>
                            <button class="btn btn-rounded btn-space btn-warning">Warning</button>
                            <button class="btn btn-rounded btn-space btn-success">Success</button>
                        </p>
                        <h4 class="xs-mt-30">With Icons</h4>
                        <p class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-rounded btn-space btn-default"><i class="icon icon-left zmdi zmdi-globe"></i> Default</button>
                            <button class="btn btn-rounded btn-space btn-primary"><i class="icon icon-left zmdi zmdi-cloud-done"></i> Primary</button>
                            <button class="btn btn-rounded btn-space btn-danger"><i class="icon icon-left zmdi zmdi-alert-circle"></i> Danger</button>
                            <button class="btn btn-rounded btn-space btn-warning"><i class="icon icon-left zmdi zmdi-alert-triangle"></i> Warning</button>
                            <button class="btn btn-rounded btn-space btn-success"><i class="icon icon-left zmdi zmdi-check"></i> Success</button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Buttons Groups<span class="panel-subtitle">These are the different buttons group component</span></div>
                    <div class="panel-body xs-mt-15">
                        <h4>Standard</h4>
                        <div class="xs-mt-20 xs-mb-30">
                            <div class="btn-toolbar">
                                <div class="btn-group btn-space">
                                    <button type="button" class="btn btn-default"><i class="icon zmdi zmdi-email"></i></button>
                                    <button type="button" class="btn btn-default"><i class="icon zmdi zmdi-cloud-outline"></i></button>
                                    <button type="button" class="btn btn-default"><i class="icon zmdi zmdi-mood"></i></button>
                                    <button type="button" class="btn btn-default"><i class="icon zmdi zmdi-favorite-outline"></i></button>
                                </div>
                                <div class="btn-group btn-space">
                                    <button type="button" class="btn btn-default"><i class="icon zmdi zmdi-mic"></i></button>
                                    <button type="button" class="btn btn-default"><i class="icon zmdi zmdi-card-giftcard"></i></button>
                                    <button type="button" class="btn btn-default"><i class="icon zmdi zmdi-portable-wifi-changes"></i></button>
                                </div>
                                <div class="btn-group btn-space">
                                    <button type="button" class="btn btn-default"><i class="icon zmdi zmdi-globe-alt"></i></button>
                                    <button type="button" class="btn btn-default"><i class="icon zmdi zmdi-settings"></i></button>
                                </div>
                            </div>
                        </div>
                        <h4 class="xs-mt-30">Default groups</h4>
                        <div class="xs-mt-20 xs-mb-10">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Left</button>
                                    <button type="button" class="btn btn-default">Mid</button>
                                    <button type="button" class="btn btn-default">Right</button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary">Left</button>
                                    <button type="button" class="btn btn-primary">Mid</button>
                                    <button type="button" class="btn btn-primary">Right</button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger">Left</button>
                                    <button type="button" class="btn btn-danger">Mid</button>
                                    <button type="button" class="btn btn-danger">Right</button>
                                </div>
                            </div>
                        </div>
                        <h4 class="xs-mt-30">Justified button</h4>
                        <div class="xs-mt-20 xs-mb-10">
                            <div class="btn-toolbar">
                                <div role="group" class="btn-group btn-group-justified xs-mb-10"><a href="#" class="btn btn-default">Home</a><a href="#" class="btn btn-default">Gallery</a><a href="#" class="btn btn-default">Services</a><a href="#" class="btn btn-default">Contact</a><a href="#" class="btn btn-default">Support</a><a href="#" class="btn btn-default">Help</a></div>
                                <div role="group" class="btn-group btn-group-justified btn-space"><a href="#" class="btn btn-primary">Home</a><a href="#" class="btn btn-primary">Gallery</a><a href="#" class="btn btn-primary">Services</a><a href="#" class="btn btn-primary">Contact</a><a href="#" class="btn btn-primary">Support</a><a href="#" class="btn btn-primary">Help</a></div>
                            </div>
                        </div>
                        <h4 class="xs-mt-30">Vertical groups</h4>
                        <div class="xs-mt-20 xs-mb-10">
                            <div class="btn-toolbar">
                                <div role="group" class="btn-group-vertical btn-space"><a href="#" class="btn btn-default">Top</a><a href="#" class="btn btn-default">Middle</a><a href="#" class="btn btn-default">Bottom</a></div>
                                <div role="group" class="btn-group-vertical btn-space"><a href="#" class="btn btn-primary">Top</a><a href="#" class="btn btn-primary">Middle</a><a href="#" class="btn btn-primary">Bottom</a></div>
                                <div role="group" class="btn-group-vertical btn-space"><a href="#" class="btn btn-danger">Top</a><a href="#" class="btn btn-danger">Middle</a><a href="#" class="btn btn-danger">Bottom</a></div>
                                <div role="group" class="btn-group-vertical btn-space"><a href="#" class="btn btn-warning">Top</a><a href="#" class="btn btn-warning">Middle</a><a href="#" class="btn btn-warning">Bottom</a></div>
                                <div role="group" class="btn-group-vertical btn-space"><a href="#" class="btn btn-success">Top</a><a href="#" class="btn btn-success">Middle</a><a href="#" class="btn btn-success">Bottom</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Big Buttons<span class="panel-subtitle">Buttons with a vertical distribution and big size</span></div>
                    <div class="panel-body">
                        <div class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-default btn-big"><i class="icon zmdi zmdi-home"></i> Home </button>
                            <button class="btn btn-space btn-default btn-big"><i class="icon zmdi zmdi-face"></i> UI </button>
                            <button class="btn btn-space btn-default btn-big"><i class="icon zmdi zmdi-inbox"></i> Email </button>
                            <button class="btn btn-space btn-default btn-big"><i class="icon zmdi zmdi-pin"></i> Maps </button>
                        </div>
                        <div class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-primary btn-big"><i class="icon zmdi zmdi-home"></i> Home </button>
                            <button class="btn btn-space btn-danger btn-big"><i class="icon zmdi zmdi-face"></i> UI </button>
                            <button class="btn btn-space btn-warning btn-big"><i class="icon zmdi zmdi-inbox"></i> Email </button>
                            <button class="btn btn-space btn-success btn-big"><i class="icon zmdi zmdi-pin"></i> Maps</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Buttons Dropdowns<span class="panel-subtitle">These are the different dropdowns styles with buttons</span></div>
                    <div class="panel-body xs-mt-15">
                        <h4>Basic dropdown</h4>
                        <div class="xs-mt-20 xs-mb-30">
                            <div class="btn-group btn-hspace">
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Default <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group btn-hspace">
                                <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Primary <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group btn-hspace">
                                <button type="button" data-toggle="dropdown" class="btn btn-danger dropdown-toggle">Danger <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group btn-hspace">
                                <button type="button" data-toggle="dropdown" class="btn btn-warning dropdown-toggle">Warning <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group btn-hspace">
                                <button type="button" data-toggle="dropdown" class="btn btn-success dropdown-toggle">Success <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="xs-mt-20 xs-mb-30">
                            <div class="btn-group btn-hspace">
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"><i class="icon icon-left zmdi zmdi-globe"></i> Icon <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group btn-hspace">
                                <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><i class="icon icon-left zmdi zmdi-cloud-done"></i> Icon <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group btn-hspace">
                                <button type="button" data-toggle="dropdown" class="btn btn-danger dropdown-toggle"><i class="icon icon-left zmdi zmdi-alert-circle"></i> Icon <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group btn-hspace">
                                <button type="button" data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><i class="icon icon-left zmdi zmdi-alert-triangle"></i> Icon <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                            <div class="btn-group btn-hspace">
                                <button type="button" data-toggle="dropdown" class="btn btn-success dropdown-toggle"><i class="icon icon-left zmdi zmdi-check"></i> Icon <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </div>
                        </div>
                        <h4>Split state dropdown</h4>
                        <div class="xs-mt-20 xs-mb-30">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default">Default</button>
                                <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle"><span class="zmdi zmdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                <ul role="menu" class="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary">Primary</button>
                                    <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="zmdi zmdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-danger">Danger</button>
                                    <button type="button" data-toggle="dropdown" class="btn btn-danger dropdown-toggle"><span class="zmdi zmdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-warning">Warning</button>
                                    <button type="button" data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><span class="zmdi zmdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-success">Success</button>
                                    <button type="button" data-toggle="dropdown" class="btn btn-success dropdown-toggle"><span class="zmdi zmdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="xs-mt-20 xs-mb-30">
                                <div class="btn-group btn-space">
                                    <button type="button" class="btn btn-default">Primary</button>
                                    <button type="button" data-toggle="dropdown" class="btn btn-primary dropdown-toggle"><span class="zmdi zmdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group btn-space">
                                    <button type="button" class="btn btn-default">Danger</button>
                                    <button type="button" data-toggle="dropdown" class="btn btn-danger dropdown-toggle"><span class="zmdi zmdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group btn-space">
                                    <button type="button" class="btn btn-default">Warning</button>
                                    <button type="button" data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><span class="zmdi zmdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                                <div class="btn-group btn-space">
                                    <button type="button" class="btn btn-default">Success</button>
                                    <button type="button" data-toggle="dropdown" class="btn btn-success dropdown-toggle"><span class="zmdi zmdi-chevron-down"></span><span class="sr-only">Toggle Dropdown</span></button>
                                    <ul role="menu" class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Separated link</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Social Buttons<span class="panel-subtitle">These are the social buttons color styles</span></div>
                    <div class="panel-body">
                        <div class="xs-mt-10 xs-mb-10">
                            <button type="button" class="btn btn-space btn-default btn-social"><i class="icon zmdi zmdi-facebook"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social"><i class="icon zmdi zmdi-google-plus"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social"><i class="icon zmdi zmdi-twitter"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social"><i class="icon zmdi zmdi-dribbble"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social"><i class="icon zmdi zmdi-dropbox"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social"><i class="icon zmdi zmdi-evernote"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social"><i class="icon zmdi zmdi-youtube-play"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social"><i class="icon zmdi zmdi-pinterest"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social"><i class="icon zmdi zmdi-github-alt"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social"><i class="icon zmdi zmdi-tumblr"></i></button>
                        </div>
                        <div class="xs-mt-10 xs-mb-10">
                            <button type="button" class="btn btn-space btn-default btn-social btn-facebook"><i class="icon zmdi zmdi-facebook"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social btn-google-plus"><i class="icon zmdi zmdi-google-plus"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social btn-twitter"><i class="icon zmdi zmdi-twitter"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social btn-dribbble"><i class="icon zmdi zmdi-dribbble"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social btn-dropbox"><i class="icon zmdi zmdi-dropbox"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social btn-evernote"><i class="icon zmdi zmdi-evernote"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social btn-youtube"><i class="icon zmdi zmdi-youtube-play"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social btn-pinterest"><i class="icon zmdi zmdi-pinterest"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social btn-github"><i class="icon zmdi zmdi-github-alt"></i></button>
                            <button type="button" class="btn btn-space btn-default btn-social btn-tumblr"><i class="icon zmdi zmdi-tumblr"></i></button>
                        </div>
                        <div class="xs-mt-10 xs-mb-10">
                            <button type="button" class="btn btn-space btn-social btn-color btn-facebook"><i class="icon zmdi zmdi-facebook"></i></button>
                            <button type="button" class="btn btn-space btn-social btn-color btn-google-plus"><i class="icon zmdi zmdi-google-plus"></i></button>
                            <button type="button" class="btn btn-space btn-social btn-color btn-twitter"><i class="icon zmdi zmdi-twitter"></i></button>
                            <button type="button" class="btn btn-space btn-social btn-color btn-dribbble"><i class="icon zmdi zmdi-dribbble"></i></button>
                            <button type="button" class="btn btn-space btn-social btn-color btn-dropbox"><i class="icon zmdi zmdi-dropbox"></i></button>
                            <button type="button" class="btn btn-space btn-social btn-color btn-evernote"><i class="icon zmdi zmdi-evernote"></i></button>
                            <button type="button" class="btn btn-space btn-social btn-color btn-youtube"><i class="icon zmdi zmdi-youtube-play"></i></button>
                            <button type="button" class="btn btn-space btn-social btn-color btn-pinterest"><i class="icon zmdi zmdi-pinterest"></i></button>
                            <button type="button" class="btn btn-space btn-social btn-color btn-github"><i class="icon zmdi zmdi-github-alt"></i></button>
                            <button type="button" class="btn btn-space btn-social btn-color btn-tumblr"><i class="icon zmdi zmdi-tumblr"></i></button>
                        </div>
                        <div class="xs-mt-10">
                            <div class="btn-group btn-space">
                                <button type="button" class="btn btn-color btn-social btn-facebook"><i class="icon zmdi zmdi-facebook"></i></button>
                                <button type="button" class="btn btn-default">Like</button>
                            </div>
                            <div class="btn-group btn-space">
                                <button type="button" class="btn btn-color btn-social btn-twitter"><i class="icon zmdi zmdi-twitter"></i></button>
                                <button type="button" class="btn btn-default">Follow Me</button>
                            </div>
                            <div class="btn-group btn-space">
                                <button type="button" class="btn btn-color btn-social btn-google-plus"><i class="icon zmdi zmdi-google-plus"></i></button>
                                <button type="button" class="btn btn-default">Plus One</button>
                            </div>
                            <div class="btn-group btn-space">
                                <button type="button" class="btn btn-color btn-social btn-github"><i class="icon zmdi zmdi-github-alt"></i></button>
                                <button type="button" class="btn btn-default">Fork</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Ripple Buttons<span class="panel-subtitle">Buttons with a vertical distribution and big size</span></div>
                    <div class="panel-body">
                        <div class="xs-mt-10 xs-mb-10">
                            <button class="btn btn-space btn-default btn-ripple-default">Default</button>
                            <button class="btn btn-space btn-primary btn-ripple-primary">Primary</button>
                            <button class="btn btn-space btn-danger btn-ripple-danger">Danger</button>
                            <button class="btn btn-space btn-warning btn-ripple-warning">Warning</button>
                            <button class="btn btn-space btn-success btn-ripple-success">Success</button>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Ripple Buttons with JS<span class="panel-subtitle">Buttons with a vertical distribution and big size</span></div>
                    <div class="panel-body">
                        <div class="xs-mt-10 xs-mb-10">
                            <button class="button-js ripple">Login</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script type="text/javascript">
    (function (window, $) {
  
  $(function() {
    
    
    $('.ripple').on('click', function (event) {
      event.preventDefault();
              
              var $div = $('<div/>'),
                  btnOffset = $(this).offset(),
                    xPos = event.pageX - btnOffset.left,
                    yPos = event.pageY - btnOffset.top;
              

              
              $div.addClass('ripple-effect');
              var $ripple = $(".ripple-effect");
              
              $ripple.css("height", $(this).height());
              $ripple.css("width", $(this).height());
              $div
                .css({
                  top: yPos - ($ripple.height()/2),
                  left: xPos - ($ripple.width()/2),
                  background: $(this).data("ripple-color")
                }) 
                .appendTo($(this));

              window.setTimeout(function(){
                $div.remove();
              }, 2000);
            });
            
          });
          
        })(window, jQuery);
    </script>
@endsection