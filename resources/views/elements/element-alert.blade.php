@extends('section.app')
@section('content')
    <div class="ai-content">
    <div class="page-head">
        <h2 class="page-head-title">Alerts</h2>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <!--Default Alerts-->
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Default Alerts<span class="panel-subtitle">These are the default bootstrap alerts style</span></div>
                    <div class="panel-body">
                        <div role="alert" class="alert alert-success alert-dismissible">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><span class="icon zmdi zmdi-check"></span><strong>Good!</strong> Better check yourself, you're not looking too good.
                        </div>
                        <div role="alert" class="alert alert-primary alert-dismissible">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><span class="icon zmdi zmdi-info-outline"></span><strong>Info!</strong> Better check yourself, you're not looking too good.
                        </div>
                        <div role="alert" class="alert alert-warning alert-dismissible">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><span class="icon zmdi zmdi-alert-triangle"></span><strong>Warning!</strong> Better check yourself, you're not looking too good.
                        </div>
                        <div role="alert" class="alert alert-danger alert-dismissible">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><span class="icon zmdi zmdi-close-circle-o"></span><strong>Danger!</strong> Better check yourself, you're not looking too good.
                        </div>
                    </div>
                </div>
            </div>
            <!--Contrast Alerts-->
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Contrasts Alerts<span class="panel-subtitle">Alerts with icon contrast background</span></div>
                    <div class="panel-body">
                        <div role="alert" class="alert alert-contrast alert-success alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-check"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Good!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-contrast alert-primary alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-info-outline"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Info!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-contrast alert-warning alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-alert-triangle"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Warning!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-contrast alert-danger alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-close-circle-o"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Danger!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!--Icon alert-->
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Icon Alerts<span class="panel-subtitle">Alerts with icon color background and white body</span></div>
                    <div class="panel-body">
                        <div role="alert" class="alert alert-success alert-icon alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-check"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Good!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-primary alert-icon alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-info-outline"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Info!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-warning alert-icon alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-alert-triangle"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Warning!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-danger alert-icon alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-close-circle-o"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Danger!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Icon alert border colored-->
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Border Color<span class="panel-subtitle">Alerts with icon colored background and border</span></div>
                    <div class="panel-body">
                        <div role="alert" class="alert alert-success alert-icon alert-icon-border alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-check"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Good!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-primary alert-icon alert-icon-border alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-info-outline"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Info!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-warning alert-icon alert-icon-border alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-alert-triangle"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Warning!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-danger alert-icon alert-icon-border alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-close-circle-o"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Danger!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!--White alert icon colored-->
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Icon Colored<span class="panel-subtitle">Alerts with colored icon and grey background</span></div>
                    <div class="panel-body">
                        <div role="alert" class="alert alert-success alert-icon alert-icon-colored alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-check"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Good!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-primary alert-icon alert-icon-colored alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-info-outline"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Info!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-warning alert-icon alert-icon-colored alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-alert-triangle"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Warning!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                        <div role="alert" class="alert alert-danger alert-icon alert-icon-colored alert-dismissible">
                            <div class="icon"><span class="zmdi zmdi-close-circle-o"></span></div>
                            <div class="message">
                                <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><strong>Danger!</strong> Better check yourself, you're not looking too good.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Alert Simple-->
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading panel-heading-divider">Basic Alerts<span class="panel-subtitle">Alerts with colored icon and white body</span></div>
                    <div class="panel-body">
                        <div role="alert" class="alert alert-success alert-simple alert-dismissible">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><span class="icon zmdi zmdi-check"></span><strong>Good!</strong> Better check yourself, you're not looking too good.
                        </div>
                        <div role="alert" class="alert alert-primary alert-simple alert-dismissible">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><span class="icon zmdi zmdi-info-outline"></span><strong>info!</strong> Better check yourself, you're not looking too good.
                        </div>
                        <div role="alert" class="alert alert-warning alert-simple alert-dismissible">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><span class="icon zmdi zmdi-alert-triangle"></span><strong>warning!</strong> Better check yourself, you're not looking too good.
                        </div>
                        <div role="alert" class="alert alert-danger alert-simple alert-dismissible">
                            <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true" class="zmdi zmdi-close"></span></button><span class="icon zmdi zmdi-close-circle-o"></span><strong>Danger!</strong> Better check yourself, you're not looking too good.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection()