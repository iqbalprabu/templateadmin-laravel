@extends('section.app')
@section('content')
    <div class="ai-content">
        <div class="page-head">
            <h2 class="page-head-title">Tables</h2>
        </div>
        <div class="main-content container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">Basic Tables
                            <div class="tools"><span class="icon zmdi zmdi-download"></span><span class="icon zmdi zmdi-more-vert"></span></div>
                        </div>
                        <div class="panel-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th style="width:50%;">Name</th>
                                    <th style="width:10%;">Date</th>
                                    <th class="number">Rate</th>
                                    <th class="number">Sales</th>
                                    <th class="actions"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Penelope Thornton</td>
                                    <td>23/06/2016</td>
                                    <td class="number">60%</td>
                                    <td class="number">639</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                <tr>
                                    <td>Justine Myranda</td>
                                    <td>15/05/2016</td>
                                    <td class="number">23%</td>
                                    <td class="number">235</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                <tr>
                                    <td>Benji Harper</td>
                                    <td>10/03/2016</td>
                                    <td class="number">79%</td>
                                    <td class="number">728</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                <tr>
                                    <td>Sherwood Clifford</td>
                                    <td>18/01/2016</td>
                                    <td class="number">18%</td>
                                    <td class="number">135</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">Striped Tables
                            <div class="tools"><span class="icon zmdi zmdi-download"></span><span class="icon zmdi zmdi-more-vert"></span></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-borderless">
                                <thead>
                                <tr>
                                    <th style="width:50%;">Name</th>
                                    <th style="width:10%;">Date</th>
                                    <th class="number">Rate</th>
                                    <th class="number">Sales</th>
                                    <th class="actions"></th>
                                </tr>
                                </thead>
                                <tbody class="no-border-x">
                                <tr>
                                    <td>Penelope Thornton</td>
                                    <td>23/06/2016</td>
                                    <td class="number">60%</td>
                                    <td class="number">639</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                <tr>
                                    <td>Justine Myranda</td>
                                    <td>15/05/2016</td>
                                    <td class="number">23%</td>
                                    <td class="number">235</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                <tr>
                                    <td>Benji Harper</td>
                                    <td>10/03/2016</td>
                                    <td class="number">79%</td>
                                    <td class="number">728</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                <tr>
                                    <td>Sherwood Clifford</td>
                                    <td>18/01/2016</td>
                                    <td class="number">18%</td>
                                    <td class="number">135</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--Condensed Table-->
                <div class="col-sm-6">
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">Condensed Tables
                            <div class="tools"><span class="icon zmdi zmdi-download"></span><span class="icon zmdi zmdi-more-vert"></span></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-condensed table-striped">
                                <thead>
                                <tr>
                                    <th>Operating System</th>
                                    <th class="number">Users</th>
                                    <th class="number">Rebound</th>
                                    <th class="actions"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Windows</td>
                                    <td class="number">1.580</td>
                                    <td class="number">20%</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-settings"></i></a></td>
                                </tr>
                                <tr>
                                    <td>Mac OS</td>
                                    <td class="number">1.322</td>
                                    <td class="number">55%</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-settings"></i></a></td>
                                </tr>
                                <tr>
                                    <td>Linux</td>
                                    <td class="number">850</td>
                                    <td class="number">45%</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-settings"></i></a></td>
                                </tr>
                                <tr>
                                    <td>Android</td>
                                    <td class="number">560</td>
                                    <td class="number">70%</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-settings"></i></a></td>
                                </tr>
                                <tr>
                                    <td>iOS</td>
                                    <td class="number">450</td>
                                    <td class="number">39%</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-settings"></i></a></td>
                                </tr>
                                <tr>
                                    <td>Other</td>
                                    <td class="number">317</td>
                                    <td class="number">67%</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-settings"></i></a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--Hover table-->
                <div class="col-sm-6">
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">Hover &amp; Image
                            <div class="tools"><span class="icon zmdi zmdi-download"></span><span class="icon zmdi zmdi-more-vert"></span></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width:37%;">User</th>
                                    <th style="width:36%;">Commit</th>
                                    <th>Date</th>
                                    <th class="actions"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="user-avatar"> <img src="assets/img/avatar6.png" alt="Avatar">Penelope Thornton</td>
                                    <td>Initial commit</td>
                                    <td>Aug 6, 2015</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="user-avatar"> <img src="assets/img/avatar4.png" alt="Avatar">Benji Harper</td>
                                    <td>Main structure markup</td>
                                    <td>Jul 28, 2015</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="user-avatar"> <img src="assets/img/avatar5.png" alt="Avatar">Justine Myranda</td>
                                    <td>Left sidebar adjusments</td>
                                    <td>Jul 15, 2015</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="user-avatar"> <img src="assets/img/avatar3.png" alt="Avatar">Sherwood Clifford</td>
                                    <td>Topbar dropdown style</td>
                                    <td>Jun 30, 2015</td>
                                    <td class="actions"><a href="#" class="icon"><i class="zmdi zmdi-delete"></i></a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--Bordered Table-->
                <div class="col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Bordered Table
                            <div class="tools"><span class="icon zmdi zmdi-download"></span><span class="icon zmdi zmdi-more-vert"></span></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-condensed table-hover table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>Operating System</th>
                                    <th class="number">Users</th>
                                    <th class="number">Rebound</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Windows</td>
                                    <td class="number">1.580</td>
                                    <td class="number">20%</td>
                                </tr>
                                <tr>
                                    <td>Mac OS</td>
                                    <td class="number">1.322</td>
                                    <td class="number">55%</td>
                                </tr>
                                <tr>
                                    <td>Linux</td>
                                    <td class="number">850</td>
                                    <td class="number">45%</td>
                                </tr>
                                <tr>
                                    <td>Android</td>
                                    <td class="number">560</td>
                                    <td class="number">70%</td>
                                </tr>
                                <tr>
                                    <td>iOS</td>
                                    <td class="number">450</td>
                                    <td class="number">39%</td>
                                </tr>
                                <tr>
                                    <td>Other</td>
                                    <td class="number">317</td>
                                    <td class="number">67%</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--Hover table-->
                <div class="col-sm-6">
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">Contextual Classes
                            <div class="tools"><span class="icon zmdi zmdi-download"></span><span class="icon zmdi zmdi-more-vert"></span></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-borderless table-hover">
                                <thead>
                                <tr>
                                    <th style="width:32%;">Class</th>
                                    <th style="width:45%;">Commit</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Penelope Thornton</td>
                                    <td>Init	</td>
                                    <td>May 6, 2016</td>
                                </tr>
                                <tr class="primary">
                                    <td>Benji Harper</td>
                                    <td>Main structure markup</td>
                                    <td>April 22, 2016</td>
                                </tr>
                                <tr>
                                    <td>Justine Myranda</td>
                                    <td>Left sidebar adjusments</td>
                                    <td>April 15, 2016</td>
                                </tr>
                                <tr class="success">
                                    <td>Sherwood Clifford</td>
                                    <td>Topbar dropdown style</td>
                                    <td>April 8, 2016</td>
                                </tr>
                                <tr>
                                    <td>Kristopher Donny</td>
                                    <td>Left sidebar adjusments</td>
                                    <td>April 15, 2016</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <!--Responsive table-->
                <div class="col-sm-12">
                    <div class="panel panel-default panel-table">
                        <div class="panel-heading">Responsive Table
                            <div class="tools"><span class="icon zmdi zmdi-download"></span><span class="icon zmdi zmdi-more-vert"></span></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th style="width:5%;">
                                        <div class="ai-checkbox ai-checkbox-sm">
                                            <input id="check1" type="checkbox">
                                            <label for="check1"></label>
                                        </div>
                                    </th>
                                    <th style="width:20%;">User</th>
                                    <th style="width:17%;">Last Commit</th>
                                    <th style="width:15%;">Milestone</th>
                                    <th style="width:10%;">Branch</th>
                                    <th style="width:10%;">Date</th>
                                    <th style="width:10%;"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <div class="ai-checkbox ai-checkbox-sm">
                                            <input id="check2" type="checkbox">
                                            <label for="check2"></label>
                                        </div>
                                    </td>
                                    <td class="user-avatar cell-detail user-info"><img src="assets/img/avatar6.png" alt="Avatar"><span>Penelope Thornton</span><span class="cell-detail-description">Developer</span></td>
                                    <td class="cell-detail"> <span>Initial commit</span><span class="cell-detail-description">Bootstrap Admin</span></td>
                                    <td class="milestone"><span class="completed">8 / 15</span><span class="version">v1.2.0</span>
                                        <div class="progress">
                                            <div style="width: 45%" class="progress-bar progress-bar-primary"></div>
                                        </div>
                                    </td>
                                    <td class="cell-detail"><span>master</span><span class="cell-detail-description">63e8ec3</span></td>
                                    <td class="cell-detail"><span>May 6, 2016</span><span class="cell-detail-description">8:30</span></td>
                                    <td class="text-right">
                                        <div class="btn-group btn-hspace">
                                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Open <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                            <ul role="menu" class="dropdown-menu pull-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="online">
                                    <td>
                                        <div class="ai-checkbox ai-checkbox-sm">
                                            <input id="check3" type="checkbox">
                                            <label for="check3"></label>
                                        </div>
                                    </td>
                                    <td class="user-avatar cell-detail user-info"><img src="assets/img/avatar4.png" alt="Avatar"><span>Benji Harper</span><span class="cell-detail-description">Designer</span></td>
                                    <td class="cell-detail"> <span>Main structure markup</span><span class="cell-detail-description">CLI Connector</span></td>
                                    <td class="milestone"><span class="completed">22 / 30</span><span class="version">v1.1.5</span>
                                        <div class="progress">
                                            <div style="width: 75%" class="progress-bar progress-bar-primary"></div>
                                        </div>
                                    </td>
                                    <td class="cell-detail"><span>develop</span><span class="cell-detail-description">4cc1bc2</span></td>
                                    <td class="cell-detail"><span>April 22, 2016</span><span class="cell-detail-description">14:45</span></td>
                                    <td class="text-right">
                                        <div class="btn-group btn-hspace">
                                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Open <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                            <ul role="menu" class="dropdown-menu pull-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="ai-checkbox ai-checkbox-sm">
                                            <input id="check4" type="checkbox">
                                            <label for="check4"></label>
                                        </div>
                                    </td>
                                    <td class="user-avatar cell-detail user-info"><img src="assets/img/avatar5.png" alt="Avatar"><span>Justine Myranda</span><span class="cell-detail-description">Designer</span></td>
                                    <td class="cell-detail"> <span>Left sidebar adjusments</span><span class="cell-detail-description">Back-end Manager</span></td>
                                    <td class="milestone"><span class="completed">10 / 30</span><span class="version">v1.1.3</span>
                                        <div class="progress">
                                            <div style="width: 33%" class="progress-bar progress-bar-primary"></div>
                                        </div>
                                    </td>
                                    <td class="cell-detail"><span>develop</span><span class="cell-detail-description">5477993</span></td>
                                    <td class="cell-detail"><span>April 15, 2016</span><span class="cell-detail-description">10:00</span></td>
                                    <td class="text-right">
                                        <div class="btn-group btn-hspace">
                                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Open <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                            <ul role="menu" class="dropdown-menu pull-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="ai-checkbox ai-checkbox-sm">
                                            <input id="check5" type="checkbox">
                                            <label for="check5"></label>
                                        </div>
                                    </td>
                                    <td class="user-avatar cell-detail user-info"><img src="assets/img/avatar3.png" alt="Avatar"><span>Sherwood Clifford</span><span class="cell-detail-description">Developer</span></td>
                                    <td class="cell-detail"> <span>Topbar dropdown style</span><span class="cell-detail-description">Bootstrap Admin</span></td>
                                    <td class="milestone"><span class="completed">25 / 40</span><span class="version">v1.0.4</span>
                                        <div class="progress">
                                            <div style="width: 55%" class="progress-bar progress-bar-primary"></div>
                                        </div>
                                    </td>
                                    <td class="cell-detail"><span>master</span><span class="cell-detail-description">8cb98ec</span></td>
                                    <td class="cell-detail"><span>April 8, 2016</span><span class="cell-detail-description">17:24</span></td>
                                    <td class="text-right">
                                        <div class="btn-group btn-hspace">
                                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Open <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                            <ul role="menu" class="dropdown-menu pull-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="online">
                                    <td>
                                        <div class="ai-checkbox ai-checkbox-sm">
                                            <input id="check6" type="checkbox">
                                            <label for="check6"></label>
                                        </div>
                                    </td>
                                    <td class="user-avatar cell-detail user-info"><img src="assets/img/avatar.png" alt="Avatar"><span>Kristopher Donny</span><span class="cell-detail-description">Designer</span></td>
                                    <td class="cell-detail"> <span>Right sidebar adjusments</span><span class="cell-detail-description">CLI Connector</span></td>
                                    <td class="milestone"><span class="completed">38 / 40</span><span class="version">v1.0.1</span>
                                        <div class="progress">
                                            <div style="width: 98%" class="progress-bar progress-bar-primary"></div>
                                        </div>
                                    </td>
                                    <td class="cell-detail"><span>master</span><span class="cell-detail-description">65bc2da</span></td>
                                    <td class="cell-detail"><span>Mars 18, 2016</span><span class="cell-detail-description">13:02</span></td>
                                    <td class="text-right">
                                        <div class="btn-group btn-hspace">
                                            <button type="button" data-toggle="dropdown" class="btn btn-default dropdown-toggle">Open <span class="icon-dropdown zmdi zmdi-chevron-down"></span></button>
                                            <ul role="menu" class="dropdown-menu pull-right">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li class="divider"></li>
                                                <li><a href="#">Separated lin</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection