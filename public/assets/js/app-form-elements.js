var App = (function () {
  'use strict';

  App.formElements = function( ){

    //Js Code
    $(".datetimepicker").datetimepicker({
    	autoclose: true,
    	componentIcon: '.zmdi.zmdi-calendar',
    	navIcons:{
    		rightIcon: 'zmdi zmdi-chevron-right',
    		leftIcon: 'zmdi zmdi-chevron-left'
    	}
    });
    
    //Select2
    $(".select2").select2({
      width: '100%'
    });
    
    //Select2 tags
    $(".tags").select2({tags: true, width: '100%'});

    //Bootstrap Slider
    $('.bslider').bootstrapSlider();
    
  };

  return App;
})(App || {});
