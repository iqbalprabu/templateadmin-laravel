<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Start Route Form */
Route::get('/form-general', function () {
    return view('form.form-general');
});
Route::get('/form-mask', function () {
    return view('form.form-mask');
});
Route::get('/form-tinymce', function () {
    return view('form.form-tinymce');
});

Route::get('/form-multipleupload', function () {
    return view('form.form-multipleupload');
});

Route::get('tinymce/image/upload', function() {
    return view('form._image-dialog');
});

Route::post('tinymce/image/upload', 'ImageController@store');
/* End Route Form */

/* Start Route Element */
Route::get('/element-alert', function () {
    return view('elements.element-alert');
});

Route::get('/element-button', function () {
    return view('elements.element-button');
});

Route::get('/element-panel', function () {
    return view('elements.element-panel');
});

Route::get('/element-modal', function () {
    return view('elements.element-modal');
});

Route::get('/element-notification', function () {
    return view('elements.element-notification');
});

Route::get('/element-tab', function () {
    return view('elements.element-tab');
});
/* End Route Element */

/*Start Route Table*/
Route::get('/table-general', function () {
    return view('tables.table-general');
});

Route::get('/table-datatables', function () {
    return view('tables.table-datatables');
});
/*End Route Table*/

/* Start Route Pages */

Route::get('/404', function () {
    return view('pages.404');
});

Route::get('/login', function () {
    return view('pages.login');
});

Route::get('/register', function () {
    return view('pages.register');
});

Route::get('/forgot-password', function () {
    return view('pages.forgot-password');
});
/* End Route Pages */
